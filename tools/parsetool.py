#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'


#used for specific one time test to parse log file

#do we need this file?  clean up possibly


class parseTool():
    def __init__(self):
            self.errorCnt = 0
            self.toParse = [
            '1168869 Comamonadaceae bacterium JGI 00BML02C18',
            '1168870 Comamonadaceae bacterium JGI 00BML02D16',
            '1168871 Comamonadaceae bacterium JGI 00BML02E14',
            '1168872 Comamonadaceae bacterium JGI 00BML02E16',
            '1168873 Comamonadaceae bacterium JGI 00BML02E17',
            '1168874 Comamonadaceae bacterium JGI 00BML02F14',
            '1168875 Comamonadaceae bacterium JGI 00BML02F20',
            '1168876 Comamonadaceae bacterium JGI 00BML02G17',
            '1168877 Comamonadaceae bacterium JGI 00BML02G21',
            '1168878 Comamonadaceae bacterium JGI 00BML02I19',
            '1168879 Comamonadaceae bacterium JGI 00BML02K07',
            '1168880 Comamonadaceae bacterium JGI 00BML02L16',
            '1168881 Comamonadaceae bacterium JGI 00BML02L21',
            '1168882 Comamonadaceae bacterium JGI 00BML02L09',
            '1168883 Comamonadaceae bacterium JGI 00BML02N11',
            '1168884 Alteromonadaceae bacterium JGI 0MLSB01E18',
            '1168885 Acholeplasmataceae bacterium JGI 0MLSB01G10v',
            '1168886 Comamonadaceae bacterium JGI 0MLSB01H10',
            '1168887 Bryobacteraceae bacterium JGI 0MLSB01H06',
            '1168888 Alteromonadaceae bacterium JGI 0MLSB02E04',
            '1168889 Xanthobacteraceae bacterium JGI 0MLSB02E08',
            '1168890 Acholeplasmataceae bacterium JGI 0MLSB02F11',
            '1168891 Xanthobacteraceae bacterium JGI 0MLSB02G05',
            '1168892 Xanthobacteraceae bacterium JGI 0MLSB02H21',
            '1168893 Comamonadaceae bacterium JGI 0MLSB02I21',
            '1168894 Acholeplasmataceae bacterium JGI 0MLSB02J05',
            '1168895 SingleCell NoTemplateControl JGI Dnfld.BML.SYBR.02.F2',
            '1168896 SingleCell NoTemplateControl JGI Dnfld.BML.SYBR.02.H2',
            '1168897 SingleCell NoTemplateControl JGI Dnfld.MSLB.SYBR.01.M2',
            '1168898 SingleCell NoTemplateControl JGI Dnfld.MSLB.SYBR.02.D2']


    def parse(self):
        justSPsList = []
        SPlist = ''
        for myString in self.toParse:
            mytuple = myString.partition(" ")
            SPlist = SPlist + mytuple[0] + ','
        return SPlist

# -------------------------------------------------------------------------------------------

# run tests
myTest = parseTool()
listToUse = myTest.parse()
print (listToUse)