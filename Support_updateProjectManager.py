from tools.util_dbTools import dbTools


#updates program manager in FD, AP and SP
#prompts user for FD/SP ids and project manager id
#updates DB and gives link (and list) for the clarity updateProjectProcessUDFs so user can use that program to
#update UDFs

class Support_updateProjectManager():
    def __init__(self):
        self.myDB = dbTools()
        self.errorCnt = 0
        self.sComment = ""
        self.server = 'dev'
        self.updateProjectProcessURL = "http://clarity-dev01.jgi-psf.org:8180/update-udfs/updateProjectProcessUdfs/entry"

    #-----------------------------------------------------
    # get all childrend of related FDs  and delete
    # inputs:  server -  "prod"  from production (pluss-genprd1), 'dev' for development (plus-dwint1)
    def doUpdate(self):
        print("Update Project Manager")
        server = input("Is this for prod or dev: (type 'prod' or 'dev'): ")
        if server != 'prod' and server != 'dev':
            self.errorCnt = 1
            exit()
        else:
            self.server = server
            #set the url for updateProjectProcessUdfs
            if self.server == 'prod':
                self.updateProjectProcessURL = "http://clarity-prd01.jgi-psf.org:8180/update-udfs/updateProjectProcessUdfs/entry"
            else:
                self.updateProjectProcessURL = "http://clarity-dev01.jgi-psf.org:8180/update-udfs/updateProjectProcessUdfs/entry"

        self.myDB.connect(self.server)
        print("-------------------------------------------\n")
        ticketNum = input("ITSSUPP ticket number: ")
        self.sComment = 'ITSSUPP-' + ticketNum + ' auto-updated by Support_updateProjectManager.py'
        pmID = input ("\nWhat is the (new) Program Manager ID: ")
        self.updateProgamManager(pmID)

        print('\n-------  D O N E  ----------')


    # ----------------------------------
    # update program manager in
    # FD, APs,  and  SPs
    # ---------------------------------
    def updateProgamManager(self, pmID):
        fdIds = self.getFDsToProcess()

        # get all sps associated with FD
        query = 'select sp.sequencing_project_id from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdIds) + ')'

        spList = self.myDB.doQueryGetAllRows(query)
        spSet = set(spList)
        print("SPS:")
        print(spSet)

        # get all aps associated with FD
        query = 'select ap.ANALYSIS_PROJECT_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdIds) + ')'
        apList = self.myDB.doQueryGetAllRows(query)
        apSet = set (apList)
        print("ANALYSIS PROJECTS:")
        print(apSet)


        # get all fds associated with FD
        query = 'select fd.final_deliv_project_id from uss.dt_final_deliv_project fd ' \
                'where fd.final_deliv_project_id in (' + str(fdIds) + ')'
        fdList = self.myDB.doQueryGetAllRows(query)
        fdSet = set(fdList)
        print("FINAL DELIVERABLE PROJECTS:")
        print(fdSet)
        print('\n')
        message = "Ready to update FD and children with new Program Manager ID: " + pmID + "? Type 'Y' to proceed: "
        ready = input(message)
        if ready == "Y" or ready == "y":
            self.updateSPs(spSet,pmID)
            self.updateAPs(apSet,pmID)
            self.updateFDs(fdSet,pmID)
        else:
            print ("\nNo Action Taken! \n")

        query = ("select distinct "
                 "fd.final_deliv_project_id as fdid, "
                 "fd.DEFAULT_PROJECT_MANAGER_ID as fd_pm, "
                 "ap.ANALYSIS_PROJECT_ID as ap_id, "
                 "ap.PROJECT_MANAGER_ID as ap_pm, "
                 "sp.sequencing_project_id as spid, "
                 "sp.SEQUENCING_PROJECT_MANAGER_ID as sp_pm "
                 "from uss.dt_final_deliv_project fd "
                 "left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id "
                 "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id "
                 "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdIds + ");" )


        print("-------------------------------------------")
        print("\n\n---- use this query to confirm the that the project manager has been changed in USS.Dt database: ----")
        print(query)

        print("\n\n---- IMPORTANT!! DO THIS: ")
        print ("Use the following set of SPs with  updateProjectProcessUDFs controller (link provided) ")
        print  ("to change Sequencing Project Manager and Sequencing Project Manager ID from all Projects listed. ")
        print("If you need to get the Sequencing Project Manager's Name associated with the ID, use the 'Contacts Lookup' link.")
        print("UpdateProjectProcessUDFs URL: " + self.updateProjectProcessURL)
        print ("Contacts Lookup: " + "https://contacts.jgi.doe.gov/")
        print("SP List:")
        for sp in spSet:
            print(int(sp))


    #-----------------------------------------------------
    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command
    #
    def getFDsToProcess(self):
        fdId = 0
        fdORsp = input("What are the FDs (or SPs) (separate by comma): ")
        print("")
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        #print(fdCount)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
                self.errorCnt += 1
        else:
            fdId = fdORsp
        return fdId


    # ---------------------------------------------------------------------------------
    def updateSPs(self,spSet,pmID):

        # delete the sps
        print("-------------------------------------------")
        print("update sps:", spSet, "with new PM: ", pmID)
        y = ",".join(map(str, spSet))

        #set PM in sps
        query = "update uss.dt_sequencing_project  set SEQUENCING_PROJECT_MANAGER_ID =" + pmID + ","\
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where sequencing_project_id in (" + y + ") " \
                                                        "and SEQUENCING_PROJECT_MANAGER_ID not in ("+pmID+ ")"
        print(query)
        self.myDB.doUpate(query)


    # ---------------------------------------------------------------------------------
    # delete the aps
    def updateAPs(self,apSet,pmID):
        print("-------------------------------------------")
        print("update aps:", apSet, "with new PM: ", pmID)
        y = ",".join(map(str, apSet))
        query = "update uss.DT_ANALYSIS_Project   set PROJECT_MANAGER_ID =" + pmID + ","\
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where ANALYSIS_PROJECT_ID in (" + y + ") " \
                                                        "and PROJECT_MANAGER_ID not in (" + pmID + ")"
        print(query)
        self.myDB.doUpate(query)



    def updateFDs(self,fdSet,pmID):
        # ---------------------------------------------------------------------------------
        # delete the FDS
        print("-------------------------------------------")
        print("update fds:", fdSet, "with new PM: ", pmID)
        y = ",".join(map(str, fdSet))
        query = "update uss.dt_final_deliv_project  set DEFAULT_PROJECT_MANAGER_ID =" + pmID + ","\
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where FINAL_DELIV_PROJECT_ID in (" + y + ") " \
                                                      "and DEFAULT_PROJECT_MANAGER_ID not in (" + pmID + ")"
        print(query)
        self.myDB.doUpate(query)







# -------------------------------------------------------------------------------------------
# run tests
myTest = Support_updateProjectManager()
myTest.doUpdate()
errs = myTest.errorCnt

print("---Number of Errors Found = " + str(errs) + " ---")
