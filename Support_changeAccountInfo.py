#from wsTools import wsTools
#from util_udfTools import udfTools
from tools.util_dbTools import dbTools
#from util_jsonInput import jsonInput

# will allow user to change different aspects of the program account (acctid, scientific program,
#  user program, purpose, and/or year)
# user enters FDs or SPs that need to be changed (either one is ok, but not mixture)
# user is prompted to what aspect of the account info needs to change.
# user verify input choices
# each sp/FD (and children) accountID is  calculated and updated in the database


class Support_changeAccountInfo():
    def __init__(self):
        self.sComment = ''
        self.accountAttributes = {
            'current_acctID': 0,
            'new_acctID': 0,
            'sciProgramID': 0,
            'sciProgram': 0,
            'purposeID': 0,
            'purpose': 0,
            'userProgramID': 0,
            'userProgram': 0,
            'yearID': 0,
            'year': 0,
            'changed_sciProgramID': 0,
            'changed_sciProgram': 0,
            'changed_purposeID': 0,
            'changed_purpose': 0,
            'changed_userProgramID': 0,
            'changed_userProgram': 0,
            'changed_yearID': 0,
            'changed_year': 0,
            'new_sciProgramID': 0,
            'new_sciProgram': 0,
            'new_purposeID': 0,
            'new_purpose': 0,
            'new_userProgramID': 0,
            'new_userProgram': 0,
            'new_yearID': 0,
            'new_year': 0
        }

        self.fdAccount = {}
        self.myDB = dbTools()

    # -----------------------------------------------------
    # get all childrend of related FDs  and abandon
    # inputs:  server -  "prod"  from production (pluss-genprd1), 'dev' for development (plus-dwint1)
    def doChangeAccount(self):
        print("Change Acount Info on FD, SP and Sow")
        print("------------------------------------------------------------------------------------------------.")
        print("This will change all FDs, SPs,Sow to the acct number determined by user input.")
        print("------------------------------------------------------------------------------------------------\n")

        server = input("Is this for prod or dev: (type 'prod' or 'dev'): ")
        if server != 'prod':  # and server != 'dev':
            # exit()
            server = 'dev'  # default
        #self.myDB = dbTools()
        self.myDB.connect(server)
        print("-------------------------------------------\n")
        ticketNum = input("ITSSUPP ticket number: ")
        # set status comment for db update
        self.sComment = 'ITSSUPP-' + ticketNum + ' auto-updated by Support_changeAccountInfo.py'
        fdIdList = set(self.getFDsToProcess())
        if len(fdIdList) > 0:
            print ("the list has this many items:",len(fdIdList))
            print (fdIdList)
            first = True
            newAcctID = 0
            # for each fd, get account info
            for fd in fdIdList:
                # print ("FD ID: ",fd)
                # get current acct id
                fdAcctID = self.getFDAcctID(fd)
                self.accountAttributes['current_acctID'] = fdAcctID
                self.getAccountAttributes(fdAcctID)

                # get new account id based on individual attribute changes, i.e. scientific prog, purpose, user, year
                newAcctID = self.determineNewAcctID(first)
                # print (newAcctID)
                self.accountAttributes['new_acctID'] = newAcctID

                self.fdAccount[fd] = self.accountAttributes
                #print(self.fdAccount[fd])
                myFDattributes = self.fdAccount[fd]

                print("----------------------------------------------------------------------")
                print("FD ID: ", fd)
                print("Changing acct ID from " + myFDattributes['current_acctID'] + " to " + newAcctID)
                print("     current account id = " + myFDattributes['current_acctID'] +
                      " [science program = " + myFDattributes['sciProgram'] +
                      ", purpose = " + myFDattributes['purpose'] +
                      ", user program = " + myFDattributes['userProgram'] +
                      ", year = " + myFDattributes['year'] + "]")

                print("     new account id = " + self.accountAttributes['new_acctID'] +
                      " [science program = " + self.accountAttributes['new_sciProgram'] +
                      ", purpose = " + self.accountAttributes['new_purpose'] +
                      ", user program = " + self.accountAttributes['new_userProgram'] +
                      ", year = " + self.accountAttributes['new_year'] +
                      "]")

                answer = input("OK to proceed? ('n' to cancel): ")
                if (answer != 'n'):
                    # update db with new account Id for FD, SP, sows
                    print("")
                    self.updateAcctIDforFD_SPs_Sows(fd, newAcctID)
                first = False

            print("----------------------------------------------------------------------")
            #print out query so user can verify change in DB
            fdIDListStr = ', '.join(str(e) for e in list(fdIdList))
            #print (fdIDListStr)
            query = ("select distinct "
                     "fd.final_deliv_project_id as fdid, "
                     "fd.default_account_id as fdacctid, "
                     "sp.sequencing_project_id as spid, "
                     "sp.SEQUENCING_PROJECT_NAME as name, "
                     "sp.default_account_id as sp_acctid, "
                     "sow.SOW_ITEM_ID as sowid,"
                     "sow.account_id as sowacctid,"
                     "acct.SCIENTIFIC_PROGRAM_ID as sci_progid,"
                     "sci.SCIENTIFIC_PROGRAM as sciprog,"
                     "acct.PURPOSE_ID as purid,  "
                     "pur.PURPOSE,  "
                     " acct.USER_PROGRAM_ID as user_prog,"
                     "usr.USER_PROGRAM, "
                     "acct.YEAR_ID as yearId,   "
                     "year.year "
                     "from uss.dt_final_deliv_project fd "
                     "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id "
                     "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                     "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                     "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                     "left join uss.dt_account acct on fd.DEFAULT_ACCOUNT_ID=acct.ACCOUNT_ID "
                     "left join uss.DT_SCIENTIFIC_PROGRAM_CV sci on sci.SCIENTIFIC_PROGRAM_ID=acct.SCIENTIFIC_PROGRAM_ID "
                     "left join USS.DT_PURPOSE_CV pur on pur.PURPOSE_ID = acct.PURPOSE_ID "
                     "left join USS.DT_USER_PROGRAM_CV usr on usr.USER_PROGRAM_ID = acct.USER_PROGRAM_ID "
                     "left join USS.DT_YEAR_CV year on year.YEAR_ID = acct.YEAR_ID "
                    "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdIDListStr + ");")

            print("\n\n----use this query to confirm the account info updated:----")
            print(query)
        else:
            print ("No SPs or FDs to process")
        print('\n------- D O N E ----------')
        return 0


    # -----------------------------------------------------


    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command  lineinput
    # inputs: none   # use for test: 1147848

    def getFDsToProcess(self):
        fdId = 0
        fdORsp = input("What are the FDs (or SPs)? (separate by comma): ")
        return self.processFDsorSPs(fdORsp)

    def processFDsorSPs(self, fdORsp):
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        fdIdList = fdORsp.split(",")
        #print(fdCount)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            print(spCountquery)
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                #print (query)
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                # fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
        print(fdCount)
        return fdIdList


    # -----------------------------------------------------
    # get FD account ID
    # inputs: fdID,
    # output  acctid

    def getFDAcctID(self, fdID):
        query = "select fd.default_account_id " \
                "from uss.dt_final_deliv_project fd where  fd.final_deliv_project_id in  (" + fdID + ")"
        acctID = self.myDB.doQuery(query)  # get the acct id for  FDs
        return acctID


    # -----------------------------------------------------
    # update FD, SP and sow account ID
    # inputs: fdID, new acctid
    # output none

    def updateAcctIDforFD_SPs_Sows(self, fdID, acctID):
        # update FD acct id
        statement = "update dt_final_deliv_project " \
                    "set default_account_id=  " + acctID + "," \
                    "status_comments = '" + self.sComment + "' where final_deliv_project_id in (" + fdID + ")"
        print (statement)
        self.myDB.doUpate(statement)  # update acct id for  FD

        # update SP account ids
        statement = "update dt_sequencing_project " \
                    "set default_account_id=  " + acctID + "," \
                    "status_comments = '" + self.sComment + "' where final_deliv_project_id in (" + fdID + ")"
        print(statement)
        self.myDB.doUpate(statement)  # update acct id for related SPs

        # update sows account ids
        # first get the list of associated sows
        query = 'select sow.SOW_ITEM_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdID) + ')'
        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        # print("SOWS:")
        # print(sowSet)
        allSows = ",".join(map(str, sowSet))  # create a string of sows items separated by comma
        statement = "update dt_sow_item " \
                    "set account_id=  " + acctID + "," \
                    "status_comments = '" + self.sComment + "' where sow_item_id in (" + allSows + ") "
        print(statement)
        self.myDB.doUpate(statement)  # update acct id for related Sows


    # -----------------------------------------------------
    # get  indivial attributes of account and stores in global accountAttributes dict,
    # inputs: acctID
    # output : updates accountAttributes dict

    def getAccountAttributes(self, acctID):
        # get scientific program
        # get sci prog id
        query = "select SCIENTIFIC_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        print (query)
        sciprgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['sciProgramID'] = sciprgID
        #print ("scientific program id = ", sciprgID)
        # get cv value
        query = "select SCIENTIFIC_PROGRAM from uss.DT_SCIENTIFIC_PROGRAM_CV where SCIENTIFIC_PROGRAM_ID  in (" + sciprgID + ")"
        sciprg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['sciProgram'] = sciprg
        # print("scientific program  = ", sciprg)

        # get purpose
        # get purpose id
        query = "select PURPOSE_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        purposeID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['purposeID'] = purposeID
        # print ("PURPOSE_ID = ", purposeID)
        # get cv value
        query = "select PURPOSE from uss.DT_PURPOSE_CV where PURPOSE_ID  in (" + purposeID + ")"
        purpose = self.myDB.doQuery(query)  # get info
        self.accountAttributes['purpose'] = purpose
        # print("purpose  = ", purpose)

        # get user program
        # get user_program_id
        query = "select USER_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        userPrgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['userProgramID'] = userPrgID
        # print ("USER_PROGRAM_ID = ", userPrgID)
        # get cv value
        query = "select USER_PROGRAM from uss.DT_USER_PROGRAM_CV where USER_PROGRAM_ID  in (" + userPrgID + ")"
        userPrg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['userProgram'] = userPrg
        # print("user program  = ", userPrg)

        # get YEAR
        # get YEAR_id
        query = "select YEAR_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        yearID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['yearID'] = yearID
        # print("YEAR_ID = ", yearID)
        # get cv value
        query = "select YEAR from uss.DT_YEAR_CV where YEAR_ID  in (" + yearID + ")"
        year = self.myDB.doQuery(query)  # get info
        self.accountAttributes['year'] = year
        # print("year  = ", year, type(year))

        self.accountAttributes['new_acctID'] = acctID
        '''print("current account id = " + acctID + " [science program = " + sciprgID + "(" + sciprg +
              "), purpose = " + purposeID + "(" + purpose +
              "), user program = " + userPrgID + "(" + userPrg +
              "), year = " + yearID + "(" + year + ")]")'''




    # -----------------------------------------------------
    # get  indivial attributes of account and stores in global accountAttributes dict,
    # inputs: acctID
    # output : updates accountAttributes dict

    def getNewAccountAttributes(self, acctID):
        # get scientific program
        # get sci prog id
        query = "select SCIENTIFIC_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        sciprgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_sciProgramID'] = sciprgID
        self.accountAttributes['changed_sciProgramID'] = sciprgID
        # print ("scientific program id = ", sciprgID)
        # get cv value
        query = "select SCIENTIFIC_PROGRAM from uss.DT_SCIENTIFIC_PROGRAM_CV where SCIENTIFIC_PROGRAM_ID  in (" + sciprgID + ")"
        sciprg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_sciProgram'] = sciprg
        self.accountAttributes['changed_sciProgram'] = sciprg
        # print("scientific program  = ", sciprg)

        # get purpose
        # get purpose id
        query = "select PURPOSE_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        purposeID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_purposeID'] = purposeID
        self.accountAttributes['changed_purposeID'] = purposeID
        # print ("PURPOSE_ID = ", purposeID)
        # get cv value
        query = "select PURPOSE from uss.DT_PURPOSE_CV where PURPOSE_ID  in (" + purposeID + ")"
        purpose = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_purpose'] = purpose
        self.accountAttributes['changed_purpose'] = purpose
        # print("purpose  = ", purpose)

        # get user program
        # get user_program_id
        query = "select USER_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        userPrgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_userProgramID'] = userPrgID
        self.accountAttributes['changed_userProgramID'] = userPrgID
        # print ("USER_PROGRAM_ID = ", userPrgID)
        # get cv value
        query = "select USER_PROGRAM from uss.DT_USER_PROGRAM_CV where USER_PROGRAM_ID  in (" + userPrgID + ")"
        userPrg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_userProgram'] = userPrg
        self.accountAttributes['changed_userProgram'] = userPrg
        # print("user program  = ", userPrg)

        # get YEAR
        # get YEAR_id
        query = "select YEAR_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        yearID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_yearID'] = yearID
        self.accountAttributes['changed_yearID'] = yearID
        # print("YEAR_ID = ", yearID)
        # get cv value
        query = "select YEAR from uss.DT_YEAR_CV where YEAR_ID  in (" + yearID + ")"
        year = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_year'] = year
        self.accountAttributes['changed_year'] = year
        # print("year  = ", year, type(year))

        print("new account id = " + acctID + " [science program = " + sciprgID + "(" + sciprg +
              "), purpose = " + purposeID + "(" + purpose +
              "), user program = " + userPrgID + "(" + userPrg +
              "), year = " + yearID + "(" + year + ")]")







    # -----------------------------------------------------
    # determine New account id from the individual  attributes ,
    # inputs: user inputs attributes to be changed and a new acctID will be determined and returned
    # output : new account id

    def determineNewAcctID(self, firstTime):
        newAcctID = 0

        # change individual attributes of account id
        if firstTime:  # get input
            answer = input("Change acct id? y/n: ")
            if (answer == 'y'):  # get  new account only once,  do same for all projects
                newAcctID = input("This will be applied to all FDs/SPs inputted. What is the new account id?: ")
                print("Changing acct ID to " + newAcctID)
                self.getNewAccountAttributes(newAcctID)
            else:
                self.getChangedAttributeFromUser()
                newAcctID = self.getAccountByQuery()
        else:
            newAcctID = self.getAccountByQuery()
        return newAcctID

    # -----------------------------------------------------
    # getAccountByQuery   using current attributes and changed attributes, query DB to get account id
    # inputs: db obj
    # output : new account id

    def getAccountByQuery(self):
        # scientific program
        if self.accountAttributes['changed_sciProgramID']:
            newScienticProgID = self.accountAttributes['changed_sciProgramID']
            self.accountAttributes['new_sciProgramID'] = self.accountAttributes['changed_sciProgramID']
            self.accountAttributes['new_sciProgram'] = self.accountAttributes['changed_sciProgram']
        else:  # no changes,  store in new_
            newScienticProgID = self.accountAttributes['sciProgramID']
            self.accountAttributes['new_sciProgramID'] = self.accountAttributes['sciProgramID']
            self.accountAttributes['new_sciProgram'] = self.accountAttributes['sciProgram']
        # purpose
        if self.accountAttributes['changed_purposeID']:
            newPurposeID = self.accountAttributes['changed_purposeID']
            self.accountAttributes['new_purposeID'] = self.accountAttributes['changed_purposeID']
            self.accountAttributes['new_purpose'] = self.accountAttributes['changed_purpose']
        else:  # no changes,  store in new_
            newPurposeID = self.accountAttributes['purposeID']
            self.accountAttributes['new_purposeID'] = self.accountAttributes['purposeID']
            self.accountAttributes['new_purpose'] = self.accountAttributes['purpose']

        # user program
        if self.accountAttributes['changed_userProgramID']:
            newUserPrgID = self.accountAttributes['changed_userProgramID']
            self.accountAttributes['new_userProgramID'] = self.accountAttributes['changed_userProgramID']
            self.accountAttributes['new_userProgram'] = self.accountAttributes['changed_userProgram']
        else:  # no changes,  store in new_
            newUserPrgID = self.accountAttributes['userProgramID']
            self.accountAttributes['new_userProgramID'] = self.accountAttributes['userProgramID']
            self.accountAttributes['new_userProgram'] = self.accountAttributes['userProgram']

        # year
        if self.accountAttributes['changed_yearID']:
            newYearID = self.accountAttributes['changed_yearID']
            self.accountAttributes['new_yearID'] = self.accountAttributes['changed_yearID']
            self.accountAttributes['new_year'] = self.accountAttributes['changed_year']
        else:  # no changes,  store in new_
            newYearID = self.accountAttributes['yearID']
            self.accountAttributes['new_yearID'] = self.accountAttributes['yearID']
            self.accountAttributes['new_year'] = self.accountAttributes['year']

        #print("Scientific Program=", newScienticProgID, "Purpose=", newPurposeID, "User Program=", newUserPrgID,"Year=", newYearID)

        # get new account id
        query = "select account_id from  uss.dt_account  where  SCIENTIFIC_PROGRAM_ID= " + newScienticProgID + \
                " and  PURPOSE_ID= " + newPurposeID + " and USER_PROGRAM_ID= " + newUserPrgID + " and YEAR_ID= " + newYearID
        # print(query)
        newAcctID = self.myDB.doQuery(query)  # get the acct id for  FDs
        #print(newAcctID)
        return newAcctID


    # -----------------------------------------------------
    # getChangedAttributeFromUser
    # determine New account id from the individual  attributes ,
    # inputs: user inputs attributes to be changed and a new acctID will be determined and returned
    # output : new account id

    def getChangedAttributeFromUser(self):
        newAcctID = 0

        # ---scientific program
        answer = input("Current Scientific Program = " + self.accountAttributes['sciProgram'] + ". Change? y/n ")
        if (answer == 'y'):
            answer = input(
                "New scientific program? \n 1=Plant \n 2=Fungal \n 3=Microbial \n 4=Metagenome \n "
                "5=Genomic Tech \n 6=JGI Science \n 7=Synthetic Biology \n 8=Metabolomics \n ---> Select #: ")
            newScienticProgID = str(answer)
            self.accountAttributes['changed_sciProgramID'] = newScienticProgID
            query = "select SCIENTIFIC_PROGRAM from uss.DT_SCIENTIFIC_PROGRAM_CV where  SCIENTIFIC_PROGRAM_ID = " + newScienticProgID
            print("")
            newScienticProg = self.myDB.doQuery(query)
            self.accountAttributes['changed_sciProgram'] = newScienticProg

        # --- purpose
        answer = input("Current Purpose = " + self.accountAttributes['purpose'] + ".  Change? y/n ")
        if (answer == 'y'):
            answer = input(
                "New purpose? \n 1=Programmatic \n 2=R&D \n 3=QC \n 4=Validation \n "
                "5=Titration \n ---> Select #: ")
            newPurposeID = str(answer)
            self.accountAttributes['changed_purposeID'] = newPurposeID
            query = "select PURPOSE from uss.DT_PURPOSE_CV where  PURPOSE_ID = " + newPurposeID
            print("")
            newPurpose = self.myDB.doQuery(query)
            self.accountAttributes['changed_purpose'] = newPurpose

        # --- user program
        answer = input("Current User Program = " + self.accountAttributes['userProgram'] + ".  Change? y/n ")
        if (answer == 'y'):
            answer = input(
                "New User Program? \n 1=CSP \n 2=Low Dose \n 3=ICBG \n 4=BRC-Multi \n "
                "5=BRC-BESC \n 6=BRC-GLBRC \n 7=BRC-JBEI \n 8=WFO \n 9=Director's Science \n 10=Legacy \n 11=Grand Challenge \n 12=ARRA \n "
                "13=Genomic Tech \n 14=GEBA \n 15=LDRD \n 16=TDP \n 17=ETOP \n 18=Synthetic Biology \n 19=CSP-EMSL \n 20=BERSS \n "
                "21=JGI-NERSC \n 22=BRC-CBI \n 23=BRC-CABBI \n 24=SPP \n ---> Select #: ")
            newUserPrgID = str(answer)
            self.accountAttributes['changed_userProgramID'] = newUserPrgID
            query = "select USER_PROGRAM from uss.DT_USER_PROGRAM_CV where  USER_PROGRAM_ID = " + newUserPrgID
            print("")
            newUserPrg = self.myDB.doQuery(query)
            self.accountAttributes['changed_userProgram'] = newUserPrg

        # --- year
        answer = input("Current Year = " + self.accountAttributes['year'] + ". Change? y/n ")
        if (answer == 'y'):
            answer = input(
                "New Year? \n 1=2001 \n 2=2002 \n 3=2003 \n 4=2004 \n "
                "5=2005 \n 6=2006 \n 7=2007 \n 8=2008 \n 9=2009 \n 10=2010 \n 11=2011 \n 12=2012 \n "
                "13=2013 \n 14=2014 \n 15=2015 \n 16=2016 \n 17=2017 \n 18=2018 \n ---> Select #: ")
            newYearID = str(answer)
            self.accountAttributes['changed_yearID'] = newYearID
            query = "select YEAR from uss.DT_YEAR_CV where  YEAR_ID = " + newYearID
            print("")
            newYear = self.myDB.doQuery(query)
            self.accountAttributes['changed_year'] = str(newYear)



#-------------------------------------------------------------------------------------------
# run tests

myTest = Support_changeAccountInfo()

myTest.doChangeAccount()
