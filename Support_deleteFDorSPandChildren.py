#from wsTools import wsTools
#from util_udfTools import udfTools
from tools.util_dbTools import dbTools
#from util_jsonInput import jsonInput
#gets the samples, sows, sps, aps, ats  and FDs that are related to FD and abandons them. Also abandon FD
#user inputs on command line:
#         server  = prod or dev
#         itssupp jira ticket number
#         the fds or sps that need to be deleted and children. if sps are given, the associated fds will also be deleted
# will output, all decendents of FD.  The update commands to delete these entities, the query to confirm status update,
# and the URL and sample list to use in routetoworkflow to remove samples from clarity  queues

class Support_deleteFDorSPandChildren():
    def __init__(self):
        self.server = 'int'
        self.myDB = dbTools()
        self.errorCnt = 0
        self.errorMessage = ""
        self.sComment = ""
        self.server = ''
        self.route2workflowURL = "http://clarity-int01.jgi-psf.org:8180/route-to-workflow"
        self.servername = ""

    #-----------------------------------------------------
    # get all childrend of related FDs  and delete
    # inputs:  server -  "prod"  from production (pluss-genprd1), 'dev' for development (plus-dwint1)
    def dodeleteAll(self):
        print("delete FD (or SP ) and Children")
        server = input("Is this for production, development or integration: (type 'prod','dev' or 'int'): ")
        if server != 'prod' and server != 'int' and server != 'dev':
            self.errorCnt = 1
            self.errorMessage = "No such server: " + server
            print(self.errorMessage)
            print("Number of Errors: ", self.errorCnt)
            exit()
        else:
            self.server = server
            # set the url for routetoworkflow
            if self.server == 'prod':
                self.route2workflowURL = "http://clarity-prd01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-prd01"
            elif self.server == 'dev':
                self.route2workflowURL = "http://clarity-dev01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-dev01"
            elif self.server == 'int':
                self.route2workflowURL = "http://clarity-int01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-int01"
            else:
                self.route2workflowURL = ""
                self.servername = ""
                self.errorCnt = 1
                self.errorMessage = "No such server!"
                exit()

            self.myDB.connect(self.server)
            print("-------------------------------------------\n")
            ticketNum = input("ITSSUPP ticket number: ")
            self.sComment = 'ITSSUPP-' + ticketNum + ' auto-updated by ITS-support-scripts_commandline project usingSupport_deleteFDorSPandChildren.py'
            function = input (" \n  1. delete FD and Children (FD,AP,AT,SP,Samples and SOWs) \n  2. delete SP and Children (SP,Samples and SOWs)\n Choose Function: ")

            if function=="1":
                self.deleteFDandChildren()
            elif function =="2":
                self.deleteSPandChildren()
            else:
                self.errorCnt = 1
                print("**** ERROR ****  Invalid Input ")

            print('\n-------  D O N E  ----------')


    # ----------------------------------
    # delete FD and Children
    # FD, APs, ATs, SPs, Samples, sows
    # ---------------------------------
    def deleteFDandChildren(self):
        print('\n')
        fdId = self.getFDsToProcess()

        # get all samples associated with FD and delete
        query = 'select sam.SAMPLE_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id where fd.final_deliv_project_id in (' + str(
            fdId) + ')'
        sampleList = self.myDB.doQueryGetAllRows(query)
        sampleSet = set(sampleList)
        print("SAMPLES:")
        print(sampleSet)

        # get all sows associated with FD and delete
        query = 'select sow.SOW_ITEM_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        print("SOWS:")
        print(sowSet)

        # get all sps associated with FD and delete
        query = 'select sp.sequencing_project_id from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        spList = self.myDB.doQueryGetAllRows(query)
        spSet = set(spList)
        print("SPS:")
        print(spSet)

        # get all aps associated with FD and delete
        query = 'select ap.ANALYSIS_PROJECT_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'
        apList = self.myDB.doQueryGetAllRows(query)
        apSet = set (apList)
        print("ANALYSIS PROJECTS:")
        print(apSet)

        # get all ats associated with FD and delete
        query = 'select at.ANALYSIS_TASK_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'left join uss.DT_ANALYSIS_TASK at on  ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        atList = self.myDB.doQueryGetAllRows(query)
        atSet = set(atList)
        print("ANALYSIS TASKS:")
        print(atList)

        # get all fds associated with FD and delete
        query = 'select fd.final_deliv_project_id from uss.dt_final_deliv_project fd ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'
        fdList = self.myDB.doQueryGetAllRows(query)
        fdSet = set(fdList)
        print("FINAL DELIVERABLE PROJECTS:")
        print(fdSet)

        ready = input("\nReady to delete FD and children? \nType 'Y' to proceed: ")
        if ready == "Y" or ready == "y":
            self.deleteSamples(sampleSet)
            self.deleteSows(sowSet)
            self.deleteSPs(spSet)
            self.deleteAPs(apSet)
            self.deleteATs(atSet)
            self.deleteFDs(fdSet)
        else:
            print ("\nNo Action Taken! \n")

        query = ("select distinct "
                 "fd.final_deliv_project_id as fdid, "
                 "fdcv.status as fd_status, "
                 "ap.ANALYSIS_PROJECT_ID as ap_id, "
                 "apcv.status as ap_status, "
                 "at.ANALYSIS_TASK_ID as at_id, "
                 "atcv.status as at_status, "
                 "sp.sequencing_project_id as spid, "
                 "spcv.STATUS as spstatus, "
                 "sp.SEQUENCING_PROJECT_NAME, "
                 "sam.SAMPLE_ID as samid, "
                 "samcv.STATUS as samstatus,"
                 "sow.SOW_ITEM_ID as sowid,"
                 "sowcv.STATUS as sowstatus "
                 "from uss.dt_final_deliv_project fd "
                 "left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id "
                 "left join uss.DT_ANALYSIS_TASK at on  ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID "
                 "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id "
                 "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                 "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                 "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                 "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.dt_sample_status_cv samcv ON samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_FINAL_DELIV_PROJ_STATUS_CV fdcv ON fdcv.STATUS_ID = fd.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_ANALYSIS_TASK_STATUS_CV atcv ON atcv.STATUS_ID = at.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_ANALYSIS_PROJECT_STATUS_CV apcv ON apcv.STATUS_ID = ap.CURRENT_STATUS_ID "
                 "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdId + ");" )


        print("-------------------------------------------")
        print("\n\n---- use this query to confirm the statuses: ----")
        print(query)

        print(
            "\n\n---- use this set of samples with  routeToWorkflow controller to remove any samples from  queues: ----")
        print("URL: " + self.route2workflowURL)
        print("Sample List:")
        for sample in sampleSet:
            print(int(sample))


    #-----------------------------------------------------
    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command
    #
    def getFDsToProcess(self):
        fdId = 0
        fdORsp = input("What are the FDs (or SPs) (separate by comma): ")
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        #print(fdCount)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
                self.errorCnt += 1
        else:
            fdId = fdORsp
        return fdId


    # -----------------------------------------------------
    # delete SP and children
    # SPs, samples and sows only
    # ------------------------------------------------------
    def deleteSPandChildren(self):
        print('\n')
        spId = self.getSPsToProcess()

        # get all samples
        query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                'where sp.sequencing_project_id in (' + str(spId) + ')'
        # print(query)
        sampleList = self.myDB.doQueryGetAllRows(query)
        sampleSet = set(sampleList)
        print("SAMPLES:")
        print(sampleSet)

        # get all sows
        query = 'select sow.SOW_ITEM_ID from uss.dt_sow_item sow ' \
                'where sow.sequencing_project_id in (' + str(spId) + ')'
        # print (query)
        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        print("SOWS:")
        print(sowSet)

        # get all sps
        query = 'select sp.sequencing_project_id from uss.dt_sequencing_project sp ' \
                'where sp.sequencing_project_id in (' + str(spId) + ')'
        spList = self.myDB.doQueryGetAllRows(query)
        spSet = set(spList)
        print("SPS:")
        print(spSet)

        ready = input("\nReady to delete SP and children? \nType 'Y' to proceed: ")
        if ready == "Y" or ready == "y":
            self.deleteSamples(sampleSet)
            self.deleteSows(sowSet)
            self.deleteSPs(spSet)

        query = ("select distinct "
                 "sp.sequencing_project_id as spid, "
                 "spcv.STATUS as spstatus, "
                 "sp.SEQUENCING_PROJECT_NAME, "
                 "sam.SAMPLE_ID as samid, "
                 "samcv.STATUS as samstatus,"
                 "sow.SOW_ITEM_ID as sowid,"
                 "sowcv.STATUS as sowstatus "
                 "from uss.dt_sequencing_project sp "
                 "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                 "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                 "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                 "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.dt_sample_status_cv samcv ON samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                 "where  sp.sequencing_project_id in (" + spId + ");")

        print("\n\n---- use this query to confirm the statuses: ----")
        print(query)

        print(
            "\n\n---- use this set of samples with  routeToWorkflow controller to remove any samples from  queues: ----")
        print("URL: " + self.route2workflowURL)
        print("Sample List:")
        for sample in sampleSet:
            print(int(sample))




    # -----------------------------------------------------
    # get the input from user,
    # separated by comma.  Will return the list of SPs associated with command
    #

    def getSPsToProcess(self):
        spList = input("What are the SPs (separate by comma): ")
        spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + spList + ")"
        spCount = self.myDB.doQuery(spCountquery)
        if int(spCount) > 0:  # sps were inputted
            spId = ",".join(map(str, set(spList)))
        else:
            print("***** no SPs found in DB ******")
            self.errorCnt += 1
            spID = []

        return spList


    #---------------------------------------------------------------------------------
    def deleteSamples(self, sampleSet):

        # delete the samples
        print("-------------------------------------------")
        print("delete samples:", sampleSet, "if not deleted already")
        y = ",".join(map(str, sampleSet))
        query = "update uss.dt_sample set current_status_id=16, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where sample_id in (" + y + ") " \
                                                                                     "and current_status_id not in (16)"
        print(query)
        self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    def deleteSows(self,sowSet):

        # delete the sows
        print("-------------------------------------------")
        print("delete sows:", sowSet, "if not deleted already")
        y = ",".join(map(str, sowSet))
        query = "update uss.dt_sow_item set current_status_id=10, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where sow_item_id in (" + y + ") " \
                                                                                       "and current_status_id not in (10)"
        print(query)
        self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    def deleteSPs(self,spSet):

        # delete the sps
        print("-------------------------------------------")
        print("delete sps:", spSet, "if not deleted already")
        y = ",".join(map(str, spSet))

        # first check that SP is in correct state, update it to 'created' so we can delete it
        #update SP status (temporarily) to 1 so it can be deleted
        query = "update uss.dt_sequencing_project  set current_status_id=1, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where sequencing_project_id in (" + y + ") " \
                                                                                                "and current_status_id not in (1,2,9)"
        print(query)
        self.myDB.doUpate(query)
        #now delete it
        query = "update uss.dt_sequencing_project  set current_status_id=9, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where sequencing_project_id in (" + y + ") " \
                                                                                                 "and current_status_id not in (9)"
        print(query)
        self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # delete the aps
    def deleteAPs(self,apSet):
        print("-------------------------------------------")
        print("delete aps:", apSet, "if not deleted already")
        y = ",".join(map(str, apSet))
        query = "update uss.DT_ANALYSIS_Project  set current_status_id=4, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where ANALYSIS_PROJECT_ID in (" + y + ") " \
                                                                                               "and current_status_id not in (4)"
        print(query)
        self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # delete the ats
    def deleteATs(self,atSet):

        print("-------------------------------------------")
        print("delete ats:", atSet, "if not deleted already")
        y = ",".join(map(str, atSet))
        query = "update uss.DT_ANALYSIS_TASK  set current_status_id=13, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where ANALYSIS_TASK_ID in (" + y + ") " \
                                                                                            "and current_status_id not in (13)"
        print(query)
        self.myDB.doUpate(query)

    def deleteFDs(self,fdSet):
        # ---------------------------------------------------------------------------------
        # delete the FDS
        print("-------------------------------------------")
        print("delete fds:", fdSet, "if not deleted already")
        y = ",".join(map(str, fdSet))
        query = "update uss.dt_final_deliv_project  set current_status_id=8, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where FINAL_DELIV_PROJECT_ID in (" + y + ") " \
                                                                                                  "and current_status_id not in (8)"
        print(query)
        self.myDB.doUpate(query)







# -------------------------------------------------------------------------------------------
# run tests
myTest = Support_deleteFDorSPandChildren()
myTest.dodeleteAll()
errs = myTest.errorCnt

print("---Number of Errors Found = " + str(errs) + " ---")
