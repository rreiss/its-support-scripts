from tkinter import *
from tkinter import ttk
from Support_changeAccountInfo import *
from deprecated_Support_abandonFDandChildren import *
from deprecated_Support_abandonSPandChildren import *

def button_go_callback(*args):
        menuChoice = str(choice.get())
        if menuChoice == "1":
            myTest = Support_changeAccountInfo()
            myTest.doChangeAccount()
        elif menuChoice == "2":
            myTest = Support_abandonFDandChildren()
            myTest.doAbandonAll()
        elif menuChoice == "3":
            myTest = Support_abandonSPandChildren()
            myTest.doAbandonAll()
        elif menuChoice == "4":
            root.quit()
        else:
            statusText.set("Please select a valid menu item")
            message.configure(fg="red")
            return
        choice_entry.delete(0,'end')


root = Tk()
root.title("ITS Support Tools")

mainframe = ttk.Frame(root, padding="10 10 10 10")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)
mainframe.pack()


statusText = StringVar(root)

choice = StringVar()
choice_entry = ttk.Entry(mainframe, width=5, textvariable=choice)
choice_entry.grid(column=1, row=2, sticky=(E))


#set up menu items
label = ttk.Label(mainframe, text="Please Choose The Desired Function:\n"
                                  "1. Change Account Info (i.e Scientific Program, Purpose, User Program, Year) \n"
                                  "2. Abandon FD and Children \n"
                                  "3. Abandon SP and Children\n"
                                  "4. Exit\n").grid(column=1, row=1, sticky=W)

button_go = ttk.Button(mainframe, text="Go", command=button_go_callback).grid(column=2, row=2, sticky=W)


message = Label(root, textvariable=statusText)
message.pack()

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

choice_entry.focus()

root.bind('<Return>', button_go_callback)
root.mainloop()