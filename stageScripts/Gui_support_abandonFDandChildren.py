from tools.util_dbTools import dbTools


#gets the samples, sows, sps, aps, ats  and FDs that are related to FD and abandons them. Also abandon FD
#user inputs on command line:
#         server  = prod or int
#         itssupp jira ticket number
#         the fds or sps that need to be abandon and children. if sps are given, the associated fds will also be abandoned
# will output, all decendents of FD.  The update commands to abandon these entities, the query to confirm status update,
# and the URL and sample list to use in routetoworkflow to remove samples from clarity  queues

class GUI_Support_abandonFDandChildren():
    myDB = dbTools()
    server = 'int'
    ticketNum = '1111'
    fdORsp = ''

    def getServer(self):
        print("Abandon FD and Children")
        self.server = input("Is this for prod or int: (type 'prod' or 'int'): ")
        if self.server != 'prod' and self.server != 'int':
            exit()

        self.myDB.connect(self.server)
        print("-------------------------------------------\n")

    def setServer(self,server):
        print("Abandon FD and Children")
        self.server = server
        if self.server != 'prod' and self.server != 'int':
            exit()
        self.myDB.connect(self.server)
        print("-------connected to database-----------------\n")




    def getJIRAticketNumber(self):
        self.ticketNum = input("ITSSUPP ticket number: ")

    def getFDsOrSPs(self):
        self.fdORsp = input("what are the Fds/Sps (separate by comma): ")


    #-----------------------------------------------------
    # get all childrend of related FDs  and abandon
    # inputs:  server -  "prod"  from production (pluss-genprd1), 'int' for integration (plus-genint1)
    def doAbandonAll(self,commandLine):

        if commandLine:
            self.getServer()
            self.getJIRAticketNumber()
            self.getFDsOrSPs()


        sComment = 'ITSSUPP-' + self.ticketNum + ' auto-updated by deprecated_Support_abandonFDandChildren.py'  # set status comment for db update

        fdId = self.processFDsOrSPs()

        # get all samples associated with FD and abandon
        query = 'select sam.SAMPLE_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id where fd.final_deliv_project_id in (' + str(
            fdId) + ')'
        sampleList = self.myDB.doQueryGetAllRows(query)
        sampleSet = set(sampleList)
        print("SAMPLES:")
        print(sampleSet)

        # get all sows associated with FD and abandon
        query = 'select sow.SOW_ITEM_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        print("SOWS:")
        print(sowSet)

        # get all sps associated with FD and abandon
        query = 'select sp.sequencing_project_id from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        spList = self.myDB.doQueryGetAllRows(query)
        spSet = set(spList)
        print("SPS:")
        print(spSet)

        # get all aps associated with FD and abandon
        query = 'select ap.ANALYSIS_PROJECT_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'
        apList = self.myDB.doQueryGetAllRows(query)
        apSet = set (apList)
        print("ANALYSIS PROJECTS:")
        print(apSet)

        # get all ats associated with FD and abandon
        query = 'select at.ANALYSIS_TASK_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'left join uss.DT_ANALYSIS_TASK at on  ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        atList = self.myDB.doQueryGetAllRows(query)
        atSet = set(atList)
        print("ANALYSIS TASKS:")
        print(atList)

        # get all fds associated with FD and abandon
        query = 'select fd.final_deliv_project_id from uss.dt_final_deliv_project fd ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'
        fdList = self.myDB.doQueryGetAllRows(query)
        fdSet = set(fdList)
        print("FINAL DELIVERABLE PROJECTS:")
        print(fdSet)

        ready = input("\nReady to Abandon FD and children? \nType 'Y' to proceed: ")

        if ready == "Y" or ready == "y":
            # abandon the samples
            print("-------------------------------------------")
            print("abandon samples:", sampleSet, "if not abandoned or deleted already")
            y = ",".join(map(str, sampleSet))
            query = "update uss.dt_sample set current_status_id=13, " \
                    "status_comments = '" + sComment + "',status_date = sysdate " \
                                                       "where sample_id in (" + y + ") " \
                                                       "and current_status_id not in (13,16)"
            print(query)
            self.myDB.doUpate(query)

            # abandon the sows
            print("-------------------------------------------")
            print("abandon sows:", sowSet, "if not abandoned or deleted already")
            y = ",".join(map(str, sowSet))
            query = "update uss.dt_sow_item set current_status_id=7, " \
                    "status_comments = '" + sComment + "',status_date = sysdate " \
                                                       "where sow_item_id in (" + y + ") " \
                                                       "and current_status_id not in (7,10,6)"
            print(query)
            self.myDB.doUpate(query)

            # abandon the sps
            print("-------------------------------------------")
            print("abandon sps:", spSet, "if not abandoned, completed or deleted already")
            y = ",".join(map(str, spSet))
            query = "update uss.dt_sequencing_project  set current_status_id=7, " \
                    "status_comments = '" + sComment + "',status_date = sysdate " \
                                                       "where sequencing_project_id in (" + y + ") " \
                                                       "and current_status_id not in (7,9,6)"
            print(query)
            self.myDB.doUpate(query)

            # abandon the aps
            print("-------------------------------------------")
            print("abandon aps:", apSet, "if not abandoned. complete or deleted already")
            y = ",".join(map(str, apSet))
            query = "update uss.DT_ANALYSIS_Project  set current_status_id=3, " \
                    "status_comments = '" + sComment + "',status_date = sysdate " \
                                                       "where ANALYSIS_PROJECT_ID in (" + y + ") " \
                                                       "and current_status_id not in (3,4,2)"
            print(query)
            self.myDB.doUpate(query)  # cancel the ats
            print("-------------------------------------------")
            print("cancel ats:", atSet, "if not cancelled, done or deleted already")
            y = ",".join(map(str, atSet))
            query = "update uss.DT_ANALYSIS_TASK  set current_status_id=10, " \
                    "status_comments = '" + sComment + "',status_date = sysdate " \
                                                       "where ANALYSIS_TASK_ID in (" + y + ") " \
                                                       "and current_status_id not in (9,10,13)"
            print(query)
            self.myDB.doUpate(query)

            # abandon the FDS
            print("-------------------------------------------")
            print("abandon fds:", fdSet, "if not abandoned or deleted already")
            y = ",".join(map(str, fdSet))
            query = "update uss.dt_final_deliv_project  set current_status_id=6, " \
                    "status_comments = '" + sComment + "',status_date = sysdate " \
                                                       "where FINAL_DELIV_PROJECT_ID in (" + y + ") " \
                                                       "and current_status_id not in (6,8,5)"
            print(query)
            self.myDB.doUpate(query)



        query = ("select distinct "
                 "fd.final_deliv_project_id as fdid, "
                 "fdcv.status as fd_status, "
                 "ap.ANALYSIS_PROJECT_ID as ap_id, "
                 "apcv.status as ap_status, "
                 "at.ANALYSIS_TASK_ID as at_id, "
                 "atcv.status as at_status, "
                 "sp.sequencing_project_id as spid, "
                 "spcv.STATUS as spstatus, "
                 "sp.SEQUENCING_PROJECT_NAME, "
                 "sam.SAMPLE_ID as samid, "
                 "samcv.STATUS as samstatus,"
                 "sow.SOW_ITEM_ID as sowid,"
                 "sowcv.STATUS as sowstatus "
                 "from uss.dt_final_deliv_project fd "
                 "left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id "
                 "left join uss.DT_ANALYSIS_TASK at on  ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID "
                 "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id "
                 "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                 "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                 "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                 "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.dt_sample_status_cv samcv ON samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_FINAL_DELIV_PROJ_STATUS_CV fdcv ON fdcv.STATUS_ID = fd.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_ANALYSIS_TASK_STATUS_CV atcv ON atcv.STATUS_ID = at.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_ANALYSIS_PROJECT_STATUS_CV apcv ON apcv.STATUS_ID = ap.CURRENT_STATUS_ID "
                 "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdId + ");" )

        print ("\n\n----use this query to confirm the statuses:----")
        print (query)

        print("\n\n----use this set of samples with  routeToWorkflow controller to remove any samples from  queues:----")
        rtwfInt = "http://clarity-int01.jgi-psf.org:8180/clarity/routeToWorkflow/entry"
        rtwfProd = "http://clarity-prd01.jgi-psf.org:8180/clarity/routeToWorkflow/entry"
        if self.server == 'prod':
            print ("URL: " + rtwfProd)
        else:
            print("URL: " + rtwfInt)
        print ("Sample List:")
        for sample in sampleSet:
            print (int(sample))



        print('\n------- D O N E ----------')
        return 0


    #-----------------------------------------------------
    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command  lineinput
    # inputs:  myDB - database object, must be opened, ready to use

    def processFDsOrSPs(self):
        fdId = 0
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + self.fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        #print(fdCount)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + self.fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + self.fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
        else:
            fdId = self.fdORsp
        return fdId


# -------------------------------------------------------------------------------------------
# run tests

'''myTest = GUI_Support_abandonFDandChildren()

errs = myTest.doAbandonAll(True)  # set to true if input from command line




print("---Number of Errors Found = " + str(errs) + " ---")'''
