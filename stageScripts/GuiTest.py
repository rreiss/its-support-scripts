from tkinter import *
from tkinter import ttk

from deprecated_Support_abandonSPandChildren import *

from stageScripts.Gui_support_abandonFDandChildren import *


class GuiTest:

    def __init__(self, master):
        self.master = master
        self.master.title("ITS Support Tools")

        mainframe = ttk.Frame(master, padding="20 20 20 20")
        mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        mainframe.columnconfigure(0, weight=1)
        mainframe.rowconfigure(0, weight=1)
        mainframe.pack()

        self.statusText = StringVar(self.master)

        self.choice = StringVar()

        # set up menu items

        label = ttk.Label(mainframe, text="Please Click on the Desired Function:\n").grid(column=1, row=1, sticky=W)
        button_changeAccountInfo = ttk.Radiobutton(mainframe, text="Change Account Info",variable=self.choice, value=1,command=self.button_go_callback).grid(column=1, row=2, sticky=W)
        button_abandonFD = ttk.Radiobutton(mainframe, text="Abandon FD and Children", variable=self.choice, value=2, command=self.button_go_callback).grid(column=1, row=3, sticky=W)
        button_abandonSP = ttk.Radiobutton(mainframe, text="Abandon SP and Children", variable=self.choice, value=3,command=self.button_go_callback).grid(column=1, row=4, sticky=W)
        button_exit = ttk.Radiobutton(mainframe, text="Exit", variable=self.choice, value=4,command=self.button_go_callback).grid(column=1, row=5, sticky=W)



        #dont totally understand this part, but it works (not sure why I cant use mainframe) dont need now
        self.message = Label(self.master, textvariable=self.statusText)
        self.message.pack()
        self.master.bind('<Return>', self.button_go_callback)


        for child in mainframe.winfo_children():
            child.grid_configure(padx=5, pady=5)





    #button handler for menu choices
    def button_go_callback(self,*args):
        menuChoice = str(self.choice.get())

        if menuChoice == "1":
            self.create_window()
            #myTest = Support_changeAccountInfo()
            #myTest.doChangeAccount()
        elif menuChoice == "2":
            self.create_window()
           # myTest = Support_abandonFDandChildren()
            #myTest.doAbandonAll()
        elif menuChoice == "3":
            self.create_window()
            #myTest = Support_abandonSPandChildren()
            #myTest.doAbandonAll()
        elif menuChoice == "4":
            self.master.quit()

        self.choice.set(9)  # clear the checked bullet  by setting it to an invalid choice

    #create window
    def create_window(self):

        newW = Toplevel(self.master)
        self.otherWindow = newW
        newW.wm_title("Abandon FD and Children")
        l = Label(newW, text="xxx")

        self.choice = StringVar()

        # set up menu items

        label = ttk.Label(newW, text="Select which server that these changes will be applied to:\n").grid(column=1, row=1, sticky=W)
        ttk.Radiobutton(newW, text="Development", variable=self.choice, value='dev',
                                                   command=self.prodORdev).grid(column=1, row=2, sticky=W)
        ttk.Radiobutton(newW, text="Production", variable=self.choice, value='prod',
                                           command=self.prodORdev).grid(column=1, row=3, sticky=W)

        ttk.Radiobutton(newW, text="Exit", variable=self.choice, value='exit',
                        command=self.prodORdev).grid(column=1, row=5, sticky=W)



        # button handler for child window
    def prodORdev (self,*args):
        server = str(self.choice.get())
        if server == 'exit':
            self.otherWindow.quit()
        myTest = GUI_Support_abandonFDandChildren()
        myTest.setServer(server)




#main code
root = Tk()
my_gui = GuiTest(root)
root.mainloop()



'''

root = Tk()


mainframe = ttk.Frame(root, padding="10 10 10 10")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)
mainframe.pack()


statusText = StringVar(root)

choice = StringVar()
choice_entry = ttk.Entry(mainframe, width=5, textvariable=choice)
choice_entry.grid(column=1, row=2, sticky=(E))


#set up menu items
label = ttk.Label(mainframe, text="Please Choose The Desired Function:\n"
                                  "1. Change Account Info (i.e Scientific Program, Purpose, User Program, Year) \n"
                                  "2. Abandon FD and Children \n"
                                  "3. Abandon SP and Children\n"
                                  "4. Exit\n").grid(column=1, row=1, sticky=W)

button_go = ttk.Button(mainframe, text="Go", command=button_go_callback).grid(column=2, row=2, sticky=W)


message = Label(root, textvariable=statusText)
message.pack()

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

choice_entry.focus()

root.bind('<Return>', button_go_callback)
root.mainloop()

'''