from jira import JIRA
import re
#this is an example of how to make JIRA call using python.
#this example updates the summary, description and a customfield for AUTOQC-1671 on JIRA test instance.

class  jiraExample():
    def __init__(self):
        options = {
            'server': 'https://issues-test.jgi-psf.org',
            'verify': False
           }
        jira = JIRA(options,basic_auth=('rreiss', 'julian1956'))
        # Get an issue.
        issue = jira.issue('AUTOQC-1671')
        issue.update(summary="Updated using PYTHON!", description="Issue UPDATED using python JIRA library - 2nd time.",customfield_13200="111-3434xxx34")




# -------------------------------------------------------------------------------------------
# run tests

myTest = jiraExample()
